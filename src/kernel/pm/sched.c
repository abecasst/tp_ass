/*
 * Copyright(C) 2011-2016 Pedro H. Penna   <pedrohenriquepenna@gmail.com>
 *              2015-2016 Davidson Francis <davidsondfgl@hotmail.com>
 *
 * This file is part of Nanvix.
 *
 * Nanvix is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nanvix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nanvix. If not, see <http://www.gnu.org/licenses/>.
 */

#include <nanvix/clock.h>
#include <nanvix/const.h>
#include <nanvix/hal.h>
#include <nanvix/pm.h>
#include <signal.h>
#include <nanvix/klib.h>
#include <nanvix/queue.h>


struct file_proc* file_ready;
struct file_proc* file_l1;
struct file_proc* file_l2;
struct file_proc* file_l3;
struct file_proc* file_l4;
int lock_multiple_queue=0;
int init_file_round_robin=0;
// 4 PRIORITY CLASSES FOR THE MULTIPLE QUEUES ALGORITHM
int init_file_l1=0;
int init_file_l2=0;
int init_file_l3=0;
int init_file_l4=0;

int nbTickets = 0;
struct process *tickets[1000];

void add_tickets_process(int n, struct process * p){
	for(int i = 0; i < n; i++){
		tickets[nbTickets] = p;
		nbTickets++;
	}
}


void remove_tickets_process(int n, struct process * p){
	for(int j=0; j < n; j++){
		for(int i = 0; i < nbTickets; i++){
			if(p == tickets[i]){
				nbTickets--;
				tickets[i]=tickets[nbTickets];
			}
		}
	}
}

void init_lottery(){
	for (struct process *p = FIRST_PROC; p <= LAST_PROC; p++)
	{
		add_tickets_process(p->nice, p);
	}
}
struct process* get_winning_process(){
	int indexRandom = ticks%nbTickets;
	return tickets[indexRandom];
}

/**
 * @brief Schedules a process to execution.
 *
 * @param proc Process to be scheduled.
 */
PUBLIC void sched(struct process *proc)
{
	proc->state = PROC_READY;
	proc->counter = 0;
}

/**
 * @brief Stops the current running process.
 */
PUBLIC void stop(void)
{
	curr_proc->state = PROC_STOPPED;
	sndsig(curr_proc->father, SIGCHLD);
	yield();
}

/**
 * @brief Resumes a process.
 *
 * @param proc Process to be resumed.
 *
 * @note The process must stopped to be resumed.
 */
PUBLIC void resume(struct process *proc)
{
	/* Resume only if process has stopped. */
	if (proc->state == PROC_STOPPED)
		sched(proc);
}

/**
 * @brief Yields the processor.
 */

PUBLIC void yield_fifo(void)
{
	struct process *p;	  /* Working process.     */
	struct process *next; /* Next process to run. */

	/* Re-schedule process for execution. */
	if (curr_proc->state == PROC_RUNNING)
		sched(curr_proc);

	/* Remember this process. */
	last_proc = curr_proc;

	/* Check alarm. */
	for (p = FIRST_PROC; p <= LAST_PROC; p++)
	{
		/* Skip invalid processes. */
		if (!IS_VALID(p))
			continue;

		/* Alarm has expired. */
		if ((p->alarm) && (p->alarm < ticks))
			p->alarm = 0, sndsig(p, SIGALRM);
	}

	/* Choose a process to run next. */
	next = IDLE;
	for (p = FIRST_PROC; p <= LAST_PROC; p++)
	{
		/* Skip non-ready process. */
		if (p->state != PROC_READY)
			continue;

		/*
		 * Process with higher
		 * waiting time found.
		 */
		if (p->counter > next->counter)
		{
			next->counter++;
			next = p;
		}

		/*
		 * Increment waiting
		 * time of process.
		 */
		else
			p->counter++;
	}

	/* Switch to next process. */
	next->priority = PRIO_USER;
	next->state = PROC_RUNNING;
	next->counter = PROC_QUANTUM;
	if (curr_proc != next)
		switch_to(next);
}


PUBLIC void yield_priority(void)
{
	struct process *p;	  /* Working process.     */
	struct process *next; /* Next process to run. */

	/* Re-schedule process for execution. */
	if (curr_proc->state == PROC_RUNNING)
		sched(curr_proc);

	/* Remember this process. */
	last_proc = curr_proc;

	/* Check alarm. */
	for (p = FIRST_PROC; p <= LAST_PROC; p++)
	{
		/* Skip invalid processes. */
		if (!IS_VALID(p))
			continue;

		/* Alarm has expired. */
		if ((p->alarm) && (p->alarm < ticks))
		{
			p->alarm = 0, sndsig(p, SIGALRM);
		}
	}
	next = IDLE;

	for (p = FIRST_PROC; p <= LAST_PROC; p++)
	{
		/* Skip non-ready process. */
		if (p->state != PROC_READY)
		{
			continue;
		}

		if (next == IDLE)
		{
			next = p;
			continue;
		}

		// Il etait mieux d'ajouter une fonction qui calcule la priorite
		if (((20 * p->nice + p->priority) < (20 * next->nice + next->priority)) || (((20 * p->nice + p->priority) == (20 * next->nice + next->priority)) && (p->counter > next->counter)))
		{
			if (next != IDLE)
			{
				next->counter++;
				next = p;
			}
		}
		else
		{
			if (p != IDLE)
			{
				p->counter++;
			}
		}

		/* Choose a process to run next. */
	}
	/* Switch to next process. */
	next->priority = PRIO_USER;
	next->state = PROC_RUNNING;
	next->counter = 0;
	switch_to(next);
}
PUBLIC void yield_round_robin(void)
{
	if(init_file_round_robin==0){
		init_queue(file_ready);
		init_file_round_robin=1;
	}
	struct process *p;	  /* Working process.     */
	struct process *next; /* Next process to run. */

	/* Re-schedule process for execution. */
	if (curr_proc->state == PROC_RUNNING)
		sched(curr_proc);

	/* Remember this process. */
	last_proc = curr_proc;

	/* Check alarm. */
	for (p = FIRST_PROC; p <= LAST_PROC; p++)
	{
		/* Skip invalid processes. */
		if (!IS_VALID(p))
			continue;

		/* Alarm has expired. */
		if ((p->alarm) && (p->alarm < ticks))
			p->alarm = 0, sndsig(p, SIGALRM);
	}

	/* Choose a process to run next. */
	next = IDLE;
	for (p = FIRST_PROC; p <= LAST_PROC; p++)
	{
		/* Skip non-ready process. */
		if (p->state != PROC_READY)
		{			
			continue;
		}
		if (!contain(file_ready,p) && p!=IDLE)
		{
			enfiler(file_ready,p);
		}
	}

		p=defiler(file_ready);
		if(p==NULL || p->state!=PROC_READY){
			p=IDLE;
		}else{
			enfiler(file_ready,p);
		}
	next =p;
	/* Switch to next process. */
	next->priority = PRIO_USER;
	next->state = PROC_RUNNING;
	next->counter = PROC_QUANTUM;
	if (curr_proc != next)
		switch_to(next);
}

PUBLIC void yield_fair_sharing(void)
{
	struct process *p;	  /* Working process.     */
	struct process *next; /* Next process to run. */

	/* Re-schedule process for execution. */
	if (curr_proc->state == PROC_RUNNING)
		sched(curr_proc);

	/* Remember this process. */
	last_proc = curr_proc;

	/* Check alarm. */
	for (p = FIRST_PROC; p <= LAST_PROC; p++)
	{
		/* Skip invalid processes. */
		if (!IS_VALID(p))
			continue;

		/* Alarm has expired. */
		if ((p->alarm) && (p->alarm < ticks))
			p->alarm = 0, sndsig(p, SIGALRM);
	}

	/* Choose a process to run next. */
	next = IDLE;
	int done = 0;
	for (p = curr_proc+1; p <= LAST_PROC; p++)
	{
		/* Skip non-ready process. */
		if (p->state == PROC_READY)
		{
			next = p;
			done = 1;
			break;
		}
	}
	if (done == 0)
	{
		for (p = FIRST_PROC; p <= curr_proc-1; p++)
		{
			/* Skip non-ready process. */
			if (p->state == PROC_READY)
			{
				next = p;
				done=1;
				break;
			}
		}
	}
	if (done==0)
	{
		next=IDLE;
	}
	/* Switch to next process. */
	next->priority = PRIO_USER;
	next->state = PROC_RUNNING;
	next->counter = PROC_QUANTUM;
	if (curr_proc != next)
		switch_to(next);
}

PUBLIC void yield_multiple_queues(void)
{
	if (init_file_l1==0){
		init_queue(file_l1);
		init_file_l1=1;
	}
	if (init_file_l2==0){
		init_queue(file_l2);
		init_file_l2=1;
	}
	if (init_file_l3==0){
		init_queue(file_l3);
		init_file_l3=1;
	}
	if (init_file_l4==0){
		init_queue(file_l4);
		init_file_l4=1;
	}

	
	


		struct process *p;	  /* Working process.     */
		struct process *next; /* Next process to run. */

		/* Re-schedule process for execution. */
		if (curr_proc->state == PROC_RUNNING)
			sched(curr_proc);

		/* Remember this process. */
		last_proc = curr_proc;

		/* Check alarm. */
		for (p = FIRST_PROC; p <= LAST_PROC; p++)
		{
			/* Skip invalid processes. */
			if (!IS_VALID(p))
				continue;

			/* Alarm has expired. */
			if ((p->alarm) && (p->alarm < ticks))
				p->alarm = 0, sndsig(p, SIGALRM);
		}

		/* Choose a process to run next. */
		next = IDLE;
		for (p = FIRST_PROC; p <= LAST_PROC; p++)
		{
			/* Skip non-ready process. */
			if (p->state != PROC_READY)
				continue;
			if (p->nice<=-20 && p->nice >= -40)
			{
				if (!contain(file_l1,p) && !contain(file_l2,p) && !contain(file_l3,p)  && !contain(file_l4,p)  )
				{
					enfiler(file_l1,p);
				}
			}
			if (p->nice<=0 && p->nice > -20)
			{
				if (!contain(file_l1,p) && !contain(file_l2,p) && !contain(file_l3,p)  && !contain(file_l4,p)  )
				{
					enfiler(file_l2,p);
				}
			}
			if (p->nice<=20 && p->nice > 0)
			{
				if (!contain(file_l1,p) && !contain(file_l2,p) && !contain(file_l3,p)  && !contain(file_l4,p)  )
				{
					enfiler(file_l3,p);
				}
			}
			if (p->nice<=40 && p->nice > 20)
			{
				if (!contain(file_l1,p) && !contain(file_l2,p) && !contain(file_l3,p)  && !contain(file_l4,p)  )
				{
					enfiler(file_l4,p);
				}
			}
			
		}
		if (lock_multiple_queue==0)
	{
		if(!file_vide(file_l1)){
			next=defiler(file_l1);
			if(next==NULL || next->state!=PROC_READY){
				next=IDLE;
			}else{
				enfiler(file_l2,next);
				lock_multiple_queue=4;
			}
			
		}else{
			if(!file_vide(file_l2)){
			next=defiler(file_l2);
			if(next==NULL || next->state!=PROC_READY){
				next=IDLE;
			}else{
				enfiler(file_l3,next);
				lock_multiple_queue=3;
			
			}
			}else{
				if(!file_vide(file_l3)){
			next=defiler(file_l3);
			if(next==NULL || next->state!=PROC_READY){
				next=IDLE;
			}else{
				enfiler(file_l4,next);
				lock_multiple_queue=2;
			}
			}else{
				if(!file_vide(file_l4)){
			next=defiler(file_l4);
			if(next==NULL || next->state!=PROC_READY){
				next=IDLE;
			}else{
				enfiler(file_l1,next);
				lock_multiple_queue=1;
			}
			}

			}

			}
/* Switch to next process. */
		
		}
	}else{
		
		if (curr_proc->state != PROC_RUNNING || curr_proc==IDLE )
		{
			lock_multiple_queue=0;
		}else{
			lock_multiple_queue--;
		}
	}
		/* Switch to next process. */
		next->priority = PRIO_USER;
		next->state = PROC_RUNNING;
		next->counter = PROC_QUANTUM;
		if (curr_proc != next)
			switch_to(next);

		
	
}
PUBLIC void yield_lottery(void)
{

	init_lottery();

	struct process *p;	  /* Working process.     */
	struct process *next; /* Next process to run. */

	/* Re-schedule process for execution. */
	if (curr_proc->state == PROC_RUNNING)
		sched(curr_proc);

	/* Remember this process. */
	last_proc = curr_proc;
	/* Check alarm. */
	for (p = FIRST_PROC; p <= LAST_PROC; p++)
	{
		/* Skip invalid processes. */
		if (!IS_VALID(p))
			continue;

		/* Alarm has expired. */
		if ((p->alarm) && (p->alarm < ticks))
			p->alarm = 0, sndsig(p, SIGALRM);
	}

	/*  choose a process to run next */
	next = get_winning_process();
	while(next->state != PROC_READY){
		next = get_winning_process();
	}

	/* Switch to next process. */
	next->priority = PRIO_USER;
	next->state = PROC_RUNNING;
	next->counter = PROC_QUANTUM;
	if (curr_proc != next)
		switch_to(next);
}

void choose_yield(int n)
{
	switch (n)
	{
	case 1:
		yield_fifo();
		break;
	case 2:
		yield_priority();
		break;
	case 3:
		yield_round_robin();
		break;
	case 4:
		yield_fair_sharing();
		break;
	case 5: 
		yield_multiple_queues();
		break;
	case 6: 
		yield_lottery();
		break;
	default:
		break;
	}
}

PUBLIC void yield(void)
{
		choose_yield(5);
	
	
	
}