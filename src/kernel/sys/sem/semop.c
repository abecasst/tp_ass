#include<nanvix/sem.h>
#include <nanvix/syscall.h>
#include <nanvix/klib.h>

PUBLIC int sys_semop(int key, int op)
{
    int index = contains_Sem_tab(key); 
   
    if (index < 0)
    {
        return -1;
    }

    if (op < 0)
    {
        down(&(Sem_tab[index]));
    }else {
        up(&Sem_tab[index]);
    }
    
    return 0;
}