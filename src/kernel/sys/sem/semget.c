#include<nanvix/sem.h>
#include <nanvix/klib.h>
#include <nanvix/syscall.h>

PUBLIC int sys_semget(int key)
{   
    int result = contains_Sem_tab(key);

    if (result < 0)
    {
       result = create(key, 0);
    }
    
    return result<0 ? -1 : key;
}