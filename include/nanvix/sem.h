#ifndef SEM_H
#define SEM_H

#define MAX_SEM_TAB 100


	struct semaphore
	{
    	int valid;
    	int key;
    	int nb_resources;
    	struct process* wait_list;
	};

	struct semaphore Sem_tab[MAX_SEM_TAB];
	
    void sem_init();
    int contains_Sem_tab (int k);
	int insert_Sem_tab();
    int create (int k, int n);
    void destroy (struct semaphore* s);
    void down (struct semaphore* s);
    void up (struct semaphore* s);

#endif
